﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace CollectIt
{
    // For HashSet
    public class EmployeeComparer : IEqualityComparer<Employee>,
                                    // For SortedSet:
                                    IComparer<Employee>
    {
        public int Compare(Employee x, Employee y)
        {
            return String.Compare(x.Name, y.Name);
        }

        public bool Equals(Employee x, Employee y)
        {
            return String.Equals(x.Name, y.Name);
        }

        public int GetHashCode(Employee obj)
        {
            return obj.Name.GetHashCode();
        }
    }

    public class DepartmentCollection : SortedDictionary<string, SortedSet<Employee>>
    {
        public DepartmentCollection Add(string departmentName, Employee employee)
        {
            if (!ContainsKey(departmentName))
            {
                Add(departmentName, new SortedSet<Employee>(new EmployeeComparer()));
            }
            this[departmentName].Add(employee);
            return this;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            var departments = new DepartmentCollection();

            departments.Add("Engineering", new Employee { Name = "Scott" })
                       .Add("Engineering", new Employee { Name = "Marcus" })
                       .Add("Engineering", new Employee { Name = "Ralph" })
                       .Add("Engineering", new Employee { Name = "Steve" })
                       .Add("Engineering", new Employee { Name = "Marco" });

            departments.Add("Marketing", new Employee { Name = "Wanda" })
                       .Add("Marketing", new Employee { Name = "Panda" })
                       .Add("Marketing", new Employee { Name = "Ronald" })
                       .Add("Marketing", new Employee { Name = "Ronald" });

            foreach (var department in departments)
            {
                Console.WriteLine(department.Key);
                foreach (var employee in department.Value)
                {
                    Console.WriteLine($"\t-{employee.Name}");
                }
            }


        }
    }
}
