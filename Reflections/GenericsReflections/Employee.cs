﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GenericsReflections
{
    public class Employee : Person, IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsValid()
        {
            return true;
        }

        public virtual void DoWork()
        {
            Console.WriteLine($"{Name}, doing real work!");
        }

        public void Speak<T>()
        {
            Console.WriteLine(typeof(T).Name);
        }

    }
}
