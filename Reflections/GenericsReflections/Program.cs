﻿using System;
using System.Collections.Generic;

namespace GenericsReflections
{
    class Program
    {
        static void Main(string[] args)
        {
            // List<> is a unbound generic.
            var employeeList = Create(typeof(List<>), typeof(Employee));
            Console.WriteLine(employeeList.GetType());
            var genericArguments = employeeList.GetType().GenericTypeArguments;
            foreach (var arg in genericArguments)
            {
                Console.WriteLine("[{0}]", arg.Name);
            }

            var employee = new Employee();
            var employeeType = typeof(Employee);
            var methodInfo = employeeType.GetMethod("Speak");
            methodInfo = methodInfo.MakeGenericMethod(typeof(DateTime));
            methodInfo.Invoke(employee, null);
        }

        private static object Create(Type collectionType, Type itemType)
        {
            var closedType = collectionType.MakeGenericType(itemType);
            return Activator.CreateInstance(closedType);
        }
    }
}
