﻿using System;
using System.Linq;

namespace GenericsReflections
{
    public interface IReadOnlyRepository<out T> : IDisposable
    {
        T FindById(int id);
        IQueryable<T> FindAll();
    }

    public interface IWriteOnlyRepository<in T> : IDisposable
    {
        void Add(T newEntity);
        void Delete(T entity);
        int Commit();
    }

    public interface IRepository<T> : IWriteOnlyRepository<T>, IReadOnlyRepository<T>, IDisposable
    {

    }
}
