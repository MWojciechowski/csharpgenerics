﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GenericsReflections
{
    interface IEntity
    {
        bool IsValid();
    }
}
