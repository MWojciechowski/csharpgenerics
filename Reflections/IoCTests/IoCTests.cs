﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GenericsReflections.Tests
{
    [TestClass]
    public class IoCTests
    {
        [TestMethod]
        public void Can_Resolve_Types()
        {
            var ioc = new Container();
            ioc.For<ILogger>().Use<SqlServerLogger>();

            var logger = ioc.Resolve<ILogger>();

            Assert.AreEqual(typeof(SqlServerLogger), logger.GetType());
        }
        [TestMethod]
        public void Can_Resolve_Types_Without_Default_Ctor()
        {
            var ioc = new Container();
            ioc.For<ILogger>().Use<SqlServerLogger>();
            ioc.For<IRepository<Employee>>().Use<SQLRepository<Employee>>();

            var repository = ioc.Resolve<IRepository<Employee>>();

            Assert.AreEqual(typeof(SQLRepository<Employee>), repository.GetType());
        }
        [TestMethod]
        public void Can_Resolve_Concrete_Type()
        {
            var ioc = new Container();
            ioc.For<ILogger>().Use<SqlServerLogger>();

	    // Whenerever type IRepository is used, use typeof SQLRepository, unbound generic use
            ioc.For(typeof(IRepository<>)).Use(typeof(SQLRepository<>));

	    
            var service = ioc.Resolve<InvoiceService>();

            Assert.IsNotNull(service);
        }
    }
}