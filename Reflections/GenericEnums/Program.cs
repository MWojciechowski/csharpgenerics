﻿using System;

namespace GenericEnums
{
    public enum Steps
    {
        Step1,
        Step2,
        Step3
    }

    public static class StringExtensions
    {
        public static TEnum ParseEnum<TEnum>(this string value) where TEnum: struct
        {
            return (TEnum)Enum.Parse(typeof(TEnum), value);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var userInput = "Step1";
            var value = userInput.ParseEnum<Steps>();
            Console.WriteLine(value);
        }
    }
}
