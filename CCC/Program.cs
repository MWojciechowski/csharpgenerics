﻿using System;
using System.Data.Entity;
using System.Linq;

namespace CCC
{
    class Program
    {
        static void Main(string[] args)
        {
            Database.SetInitializer(new DropCreateDatabaseAlways<EmployeeDb>());

            using (IRepository<Employee> eeRepository = new SQLRepository<Employee>(new EmployeeDb()))
            {
                string[] eeNamesListToCreate = new string[] {"Scott", "Marc", "Allen", "Ralph"};
                string[] managersNamesListToCreate = new string[] {"Dr Louis", "Tim"};
                AddEmployees(eeRepository, eeNamesListToCreate);
                CountEmployees(eeRepository);
                QueryEmployees(eeRepository);

                //Contravariance
                AddManagers(eeRepository, managersNamesListToCreate);
                // Covariance:
                DumpPreople(eeRepository);
            }
        }
        // 2/2 Contravariance example:
        // We want IRepository to handle Manager objects
        private static void AddManagers(IWriteOnlyRepository<Manager> eeRepository, string[] names)
        {
            foreach (var name in names)
            {
                eeRepository.Add(new Manager { Name = name });
            }
            eeRepository.Commit();
        }

        // 2/2 Covariance example:
        // We want IRepository to handle Person object.
        private static void DumpPreople(IReadOnlyRepository<Person> eeRepository)
        {
            var employees = eeRepository.FindAll();
            foreach (var ee in employees)
            {
                Console.WriteLine(ee.Name);
            }
        }

        private static void QueryEmployees(IRepository<Employee> eeRepository)
        {
            var employee = eeRepository.FindById(1);
            Console.WriteLine(employee.Name);
        }

        private static void CountEmployees(IRepository<Employee> eeRepository)
        {
            Console.WriteLine(eeRepository.FindAll().Count());
        }

        private static void AddEmployee(IRepository<Employee> eeRepository, string eeName)
        {
            eeRepository.Add(new Employee { Name = eeName });
            eeRepository.Commit();
        }
        
        private static void AddEmployees(IRepository<Employee> eeRepository, string[] eeNames)
        {
            foreach (var name in eeNames)
            {
                eeRepository.Add(new Employee { Name = name });
            }
            eeRepository.Commit();
        }
    }
}
