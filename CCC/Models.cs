using System;

namespace CCC
{
    public interface IEntity
    {
        bool IsValid();
    }
    public class Person
    {
        public string Name { get; set; }
    }

    public class Employee : Person, IEntity
    {
        public int Id { get; set; }
        public virtual void DoWork()
        {
            Console.WriteLine($"{Name}, doing real work!");
        }

        public bool IsValid()
        {
            return true;
        }
    }
    public class Manager : Employee
    {
        public override void DoWork()
        {
            Console.WriteLine($"{Name}, create a meeting...");
        }
    }

}