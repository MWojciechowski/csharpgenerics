﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;

namespace csharpgenerics
{
    public class Program
    {
        static void Main(string[] args)
        {
            // Non generic data type example:
            int[] array = new int[] {};

            // Generic ones
            List<int> list = new List<int>();
            SortedList<int, string> sortedList = new SortedList<int, string>();

            Queue<int> queue = new Queue<int>();

            Stack<int> stack = new Stack<int>();

            HashSet<int> hashSet = new HashSet<int>();

            SortedSet<int> sortedSet = new SortedSet<int>();

            LinkedList<int> linkedList = new LinkedList<int>();
            linkedList.AddFirst(1);
            var linkListFirst = linkedList.First;

            Dictionary<int, string> dictionary = new Dictionary<int, string>();
            SortedDictionary<int, string> sorteByKeyDictionary = new SortedDictionary<int, string>();

            // Concurrent generics
            ConcurrentStack<int> conStack = new ConcurrentStack<int>();
            ConcurrentQueue<int> conQueue = new ConcurrentQueue<int>();
            ConcurrentBag<int> conBag = new ConcurrentBag<int>();
            ConcurrentDictionary<int, string> conDictionary = new ConcurrentDictionary<int, string>();

            // There are also immutable collections ---SEE----> NuGet: Microsoft.Bcl.Immutable
            
        }
    }
}
