﻿using System.Collections;

namespace DataStructures
{
    public class CircularBuffer<T> : Buffer<T>
    {
        private T[] _buffer;
        private int _start;
        private int _end;

        public CircularBuffer()
            : this(capacity: 10)
        {
        }

        public CircularBuffer(int capacity)
        {
            _buffer = new T[capacity + 1];
            _start = 0;
            _end = 0;
        }

        public override void Write(T value)
        {
            _buffer[_end] = value;
            _end = (_end + 1) % _buffer.Length;
            if (_end == _start)
            {
                _start = (_start + 1) % _buffer.Length;
            }
        }

        public override T Read()
        {
            T result = _buffer[_start];
            _start = (_start + 1) % _buffer.Length;
            return result;
        }

        public int Capacity
        {
            get { return _buffer.Length; }
        }

        public override bool IsEmpty
        {
            get { return _end == _start; }
        }

        public bool IsFull
        {
            get { return (_end + 1) % _buffer.Length == _start; }
        }
    }

    // public class CircularBuffer<T> : Buffer<T>
    // {
    //     int _capacity;
    //     public CircularBuffer(int capacity = 10)
    //     {
    //         _capacity = capacity;
    //     }

    //     public override void Write(T value)
    //     {
    //         base.Write(value);
    //         if (_queue.Count > _capacity)
    //         {
    //             _queue.Dequeue();
    //         }
    //     }
    // }
}
